<?php

class Crud extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('m_data');
		$this->load->helper('url');
	}

	function index(){
		$data['dosen'] = $this->m_data->tampil_data()->result();
		$this->load->view('v_input',$data);
	}

	function tambah(){
		$this->load->view('v_input');
	}

	function tambah_aksi(){
		$nip = $this->input->post('nip');
		$nm_dosen = $this->input->post('nm_dosen');
		$phone = $this->input->post('phone');
		$id_role = $this->input->post('id_role');
 
		$data = array(
			'nip' => $nip,
			'nm_dosen' => $nm_dosen,
			'phone' => $phone,
			'id_role'=>$id_role
			);
		$this->m_data->input_data($data,'dosen');
		redirect('crud/index');
	}
 
}
 