<?php

class Jadwalkonsultasi extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('m_jadwalkonsultasi');
		$this->load->helper('url');
	}

	function index(){
		$data['detailprivatechat'] = $this->m_jadwalkonsultasi->tampil_data()->result();
		$this->load->view('v_jadwalkonsultasi',$data);
	}

	function tambah(){
		$this->load->view('v_jadwalkonsultasi');
	}

	function tambah_aksi(){
		$nip = $this->input->post('nip');
		$nm_dosen = $this->input->post('nm_dosen');
		$phone = $this->input->post('phone');
		$id_role = $this->input->post('id_role');
 
		$data = array(
			'nip' => $nip,
			'nm_dosen' => $nm_dosen,
			'phone' => $phone,
			'id_role'=>$id_role
			);
		$this->m_jadwalkonsultasi->input_data($data,'dosen');
		redirect('jadwalkonsultasi/index');
	}
 
}
 