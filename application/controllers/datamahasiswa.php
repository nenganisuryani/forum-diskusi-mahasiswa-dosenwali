<?php

class Datamahasiswa extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('m_datamahasiswa');
		$this->load->helper('url');
	}

	function index(){
		$data['mahasiswa'] = $this->m_datamahasiswa->tampil_data()->result();
		$this->load->view('v_datamahasiswa',$data);
	}

	function tambah(){
		$this->load->view('v_datamahasiswa');
	}

	function tambah_aksi(){
		$nip = $this->input->post('nip');
		$nm_dosen = $this->input->post('nm_dosen');
		$phone = $this->input->post('phone');
		$id_role = $this->input->post('id_role');
 
		$data = array(
			'nip' => $nip,
			'nm_dosen' => $nm_dosen,
			'phone' => $phone,
			'id_role'=>$id_role
			);
		$this->m_datamahasiswa->input_data($data,'dosen');
		redirect('datamahasiswa/index');
	}
 
}
