<?php

class Diskusiumum extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('m_diskusiumum');
		$this->load->helper('url');
	}

	function index(){
		$data['headdiskus'] = $this->m_diskusiumum->tampil_data()->result();
		$this->load->view('v_diskusiumum',$data);
	}

	function tambah(){
		$this->load->view('v_diskusiumum');
	}

	function tambah_aksi(){
		$nip = $this->input->post('nip');
		$nm_dosen = $this->input->post('nm_dosen');
		$phone = $this->input->post('phone');
		$id_role = $this->input->post('id_role');
 
		$data = array(
			'nip' => $nip,
			'nm_dosen' => $nm_dosen,
			'phone' => $phone,
			'id_role'=>$id_role
			);
		$this->m_diskusiumum->input_data($data,'headdiskus');
		redirect('diskusiumum/index');
	}
 
}
 