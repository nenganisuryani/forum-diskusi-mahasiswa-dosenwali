<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Forum Diskusi Mahasiswa dan Dosen Wali</title>
    <!-- Bootstrap -->
    <link href="assets/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="assets/bootstrap/css/agency.min.css" rel="stylesheet">
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forum Diskusi</title>
</head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali">Forum Diskusi</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/dosen">Profil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/datamahasiswa">Data Mahasiswa</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/diskusiumum">Diskusi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/diskusiumum">Jadwal Konsultasi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">halaman</div>
          <div class="intro-heading text-uppercase">Jadwal Konsultasi</div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#profile">Klik!</a>
        </div>
      </div>
    </header>

    <!-- Team -->
    <section class="bg-light" id="profile">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Profile Dosen</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="assets/bootstrap/img/team/1.jpg" alt="">
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="">
                    <i class="fa fa-edit"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
            <div align="left">
              <h4>Profile</h4>
              <br>
              <h5>Nama</h5>
              <h5>NIP</h5>
              <h5>Phone</h5>
              <h5>Email</h5>
              <br>
              <h4> Riwayat Pendidikan </h4>
            </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                    <i class="fa fa-edit"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </section>


    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Profile Dosen</h2>
<form action="<?php echo base_url(). 'crud/tambah_aksi'; ?>" method="post">
    <table style="margin:20px auto;">
      <tr>
        <td>Nama</td>
        <td><input type="text" nm_dosen="nm_dosen"></td>
      </tr>
      <tr>
        <td>Phone</td>
        <td><input type="text" phone="phone"></td>
      </tr>
      <tr>
      <tr>
        <td>Email</td>
        <td><input type="text" email="email"></td>
      </tr>
      <tr>
        <td>Id_role</td>
        <td><input type="text" id_role="id_role"></td>
      </tr>
        <td></td>
        <p>
        <td><input type="submit" value="Tambah"></td>
      </tr>
    </table>
  </form> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="assets/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="assets/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="assets/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="assets/bootstrap/js/jqBootstrapValidation.js"></script>
    <script src="assets/bootstrap/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="assets/bootstrap/js/agency.min.js"></script>

  </body>

</html>
