<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Forum Diskusi Mahasiswa dan Dosen Wali</title>
    <!-- Bootstrap -->
    <link href="assets/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="assets/bootstrap/css/agency.min.css" rel="stylesheet">
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forum Diskusi</title>
</head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Forum Diskusi</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/dosen">Profil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/datamahasiswa">Data Mahasiswa</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#http://localhost/forum-diskusi-mahasiswa-dosenwali/diskusiumum">Diskusi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://localhost/forum-diskusi-mahasiswa-dosenwali/jadwalkonsultasi">Jadwal Konsultasi</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">e-Discuss</div>
          <div class="intro-heading text-uppercase">Politeknik Batuninggal</div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#welcome">Klik!</a>
        </div>
      </div>
    </header>

    <!-- Team -->
    <section class="bg-light" id="welcome">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h3 class="section-heading text-uppercase">Selamat Datang di</h3>
            <h1 class="section-heading text-uppercase">Forum Diskusi Mahasiswa dan Wali Dosen</h1>
          </div>
        </div>
            </div>
          </div>
        </div>
    </section>


    <!-- Bootstrap core JavaScript -->
    <script src="assets/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="assets/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="assets/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="assets/bootstrap/js/jqBootstrapValidation.js"></script>
    <script src="assets/bootstrap/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="assets/bootstrap/js/agency.min.js"></script>

  </body>

</html>
